### Purpose ###
Sync data from AWS RDS to GCP BigQuery
### Requirement ###
1. High availability
1. 1 million records per day
1. need to handle schema change
1. data have to sync ASAP, but no later than 15 minutes
1. no data missing

### Architecture ###
The flow of data starts with the replication between RDS MySQL and GCP CloudSQL MySQL instances ([reference](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/AuroraMySQL.Replication.MySQL.html)), which is named DBZ DB here.
<br/>
DBZ DB, which consists of two MySQL machines: a primary (active) server and secondary (passive) server with [GTIDs enabled](https://dev.mysql.com/doc/refman/5.7/en/replication-gtids.html).
<br/>
Following the DBZ DB, Debezium connector (in [distributed mode](https://docs.confluent.io/current/connect/userguide.html#distributed-mode) on the Kafka connect framework) is responsible to 
1. connecting to DBZ DB and pretending to be a replica
1. DBZ DB sends its replication data to it, thinking it's actually funneling data to another downstream MySQL instance
1. read MySQL's binlog and [convert it to Kafka message format](https://docs.confluent.io/current/connect/concepts.html#converters)
1. feed the MySQL messages into Kafka
1. add schemas to the Confluent Schema Registry

Finally, Kafka connect to [BigQuery connector](https://wecode.wepay.com/posts/kafka-bigquery-connector) to load the data from Kafka to BigQuery using BigQuery’s [streaming API](https://cloud.google.com/bigquery/streaming-data-into-bigquery).
![](./image/architecture.png)

### High availability ###
Starting with MySQL 5.6, MySQL introduced the concept of global transaction IDs. These GTIDs identify a specific location within the MySQL binlog across machines. This means that a consumer reading from a binlog on one MySQL server can switch over to the other, provided that both servers have the data available. This is how we run our systems. Both the CloudSQL instances and the Debezium MySQL cluster run with GTIDs enabled. The Debezium MySQL servers also have replication binlogs enabled so that binlogs exist for Debezium to read (replicas don’t normally have binlogs enabled by default). All of this enables Debezium to consume from the primary Debezium MySQL server, but switch over to the secondary (via HA Proxy) if there’s a failure.
<br/>
<br/>
If the machine that Debezium, itself, is running on fails, then the Kafka connect framework fails the connector over to another machine in the cluster. When the failover occurs, Debezium receives its last committed offset (GTID) from Kafka connect, and picks up where it left off (with the same caveat as above: you might see some duplicate messages due to periodic commit frequency).
### Schema Change ###
Debezium's message format includes both the "before" and "after" versions of a row. For inserts, the "before" is null. For deletes, the "after" is null. Updates have both the "before" and "after" fields filled out. The messages also include some server information such as the server ID that the message came from, the GTID of the message, the server timestamp, and so on.
```json
{
  "before": {
    "id": 1004,
    "first_name": "Anne",
    "last_name": "Kretchmar",
    "email": "annek@noanswer.org"
  },
  "after": {
    "id": 1004,
    "first_name": "Anne Marie",
    "last_name": "Kretchmar",
    "email": "annek@noanswer.org"
  },
  "source": {
    "name": "mysql-server-1",
    "server_id": 223344,
    "ts_sec": 1465581,
    "gtid": null,
    "file": "mysql-bin.000003",
    "pos": 484,
    "row": 0,
    "snapshot": null
  },
  "op": "u",
  "ts_ms": 1465581029523
}
```
The serialization format that Debezium sends to Kafka is configurable. Avro is recommended for its compact size, schema DDL, performance, and rich ecosystem. Kafka connect can be configured to use Confluent’s Avro encoder codec for Kafka. This encoder serializes messages to Avro, but also registers the schemas with Confluent’s schema registry.
<br/>
<br/>
If a MySQL table's schema is changed, Debezium adapts to the change by updating the structure and schema of the "before" and "after" portions of its event messages. This will appear to the Avro encoder as a new schema, which it will register with the schema registry before the message is sent to Kafka. The registry runs full compatibility checks to make sure that downstream consumers don't break due to a schema evolution.

### Performance Consideration ###
According to the discussion in [post1](https://groups.google.com/d/msg/debezium/5KRvUeDfwQU/MpLlBTNJCAAJ) and [post2](https://gitter.im/debezium/dev?at=5a2a6de4ba39a53f1a360424).
1 million records per day would be ok.

### Data Latency ###
According to [this post](https://wecode.wepay.com/posts/streaming-databases-in-realtime-with-mysql-debezium-kafka), it said they have data latency around 30 seconds.
<br/>
Consider we have an extra effort to sync binlog from RDS to CloudSQL, addition time would be around 30 to 60 seconds.

### Avoid Data Missing ###
To avoid the data missing, we have to backup the binlog at RDS and CloudSQL for a while (ex. 7 days).
<br/>
Besides, monitoring mechanism have to be enabled as below to detect error, if error happen, we can restore the data from binlogs.
1. Monitor Replica between RDS and CloudSQL
<br/><span/>Monitor how far the replica target is behind the replication master by connecting to the replica target and running the SHOW SLAVE STATUS command. In the command output, the Seconds Behind Master field tells you how far the replica target is behind the master.
1. Monitor Kafka connect
<br/><span/>We can monitor metrics which are exposed by [Kafka Connect](https://issues.apache.org/jira/browse/KAFKA-2376) and [Debezium](https://issues.jboss.org/browse/DBZ-134)